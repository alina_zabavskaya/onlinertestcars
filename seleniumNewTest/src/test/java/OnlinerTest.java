import home.pages.HomePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.*;
import result.pages.ResultPage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;



/**
 * Created by azabavskaya on 1/30/17.
 */
public class OnlinerTest {

    private static WebDriver driver;
    private Logger logger = LoggerFactory.getLogger(OnlinerTest.class);

    @BeforeClass
    public static void setUp() {

        driver = new ChromeDriver();
//        driver = WebDriverFactory.getDriver(DesiredCapabilities);

//        DesiredCapabilities capability = DesiredCapabilities.chrome();
//
//        capability.setCapability("platform", Platform.WIN8);
//        capability.setCapability("version", "49.0");
//
//         try {
//            driver = new RemoteWebDriver(new URL("http://alinka:ee211863-6ba5-41f8-b0ab-ebb0b3d8b5ec@ondemand.saucelabs.com:80/wd/hub"), capability);
//        } catch (MalformedURLException e) {
//            e.printStackTrace();
//        }

        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//        driver.get("https://www.onliner.by/");
        driver.get("http://ab.onliner.by/");


    }

    @Test
    public void testFindCar(){


        HomePage homePage = new HomePage(driver);
//        HomePageWithAnnotations homePageWithAnnotations =  PageFactory.initElements(driver,HomePageWithAnnotations.class);
//        HomePage page1 = PageFactory.initElements(driver, HomePage.class);

        String necessaryСarMark = "Mitsubishi";
        String necessaryСarModel = "Lancer";
        int necessaryFirstYear = 2007;
        int necessarySecondYear = 2012;
        String necessaryMotorType = "Бензин";
        String necessaryTransmission = "Автоматическая";
        String necessaryColor = "Красный";

        homePage.selectCarMark(necessaryСarMark); // ok
        homePage.selectCarModel(necessaryСarModel); //ok
        homePage.selectYearRange(necessaryFirstYear,necessarySecondYear); //ok
        homePage.selectCarMotorType(necessaryMotorType); // ok
        homePage.selectTransmission(necessaryTransmission); //ok
        homePage.selectCarColor(necessaryColor); //ok

//        homePageWithAnnotations.doSelectMitsubishi(necessaryСarMark);
//        homePageWithAnnotations.doSelectLancer(necessaryСarModel);
//        homePageWithAnnotations.doSelectYearRange(necessaryFirstYear, necessarySecondYear);
//        homePageWithAnnotations.doClickCarMotorTypeByText(necessaryMotorType);
//        homePageWithAnnotations.doClickTransmissionByText(necessaryTransmission);
//        homePageWithAnnotations.doSelectRed(necessaryColor);

//        page1.selectCarMark(necessaryСarMark); // not ok

        ResultPage resultPage = new ResultPage(homePage);
//        result.pages.ResultPage resultPage1 = new result.pages.ResultPage(homePageWithAnnotations);
//        result.pages.ResultPage resultPage3 = new result.pages.ResultPage(driver);

//        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);


        Assert.assertTrue(resultPage.getCarMark().contains("Mitsubishi"));
//        Assert.assertTrue(resultPage1.getCarMark().contains("Mitsubishi"));

        Assert.assertTrue(resultPage.getCarModel().contains("Lancer"));
//        Assert.assertTrue(resultPage1.getCarModel().contains("Lancer"));

        Assert.assertTrue(resultPage.getFirstYear() == necessaryFirstYear);
//        Assert.assertTrue(resultPage1.getFirstYear() == necessaryFirstYear);

        Assert.assertTrue(resultPage.getSecondYear() == necessarySecondYear);
//        Assert.assertTrue(resultPage1.getSecondYear() == necessarySecondYear);

        Assert.assertTrue(resultPage.getMotorType().contains("Бензин"));
//        Assert.assertTrue(resultPage1.getMotorType().contains("Бензин"));

        Assert.assertTrue(resultPage.getTransmissionText().contains("Автоматическая"));
//        Assert.assertTrue(resultPage1.getTransmissionText().contains("Автоматическая"));

        Assert.assertTrue(resultPage.getColor().contains("Красный"));
//        Assert.assertTrue(resultPage1.getColor().contains("Красный"));

        Assert.assertTrue(resultPage.doCheckCarParameters(necessaryFirstYear, necessarySecondYear));
    }

    @Test
    public void testAvtobaraholkaLink() {
        WebElement webElement = driver.findElement(By.xpath(".//nav[@class = 'b-top-navigation']//span[contains(text(), 'Автобарахолка')]"));
        logger.info(webElement.getText());
        webElement.click();

        Assert.assertEquals( driver.getCurrentUrl(),"http://ab.onliner.by/");

    }

    @Test
    public void testCarModelFilter(){

        Boolean sorted = false;//change to false
        WebElement webElement = driver.findElement(By.className("manufacture"));
        webElement.click();
        logger.info("clicked");
        List<WebElement> carModels = driver.findElements(By.cssSelector(".manufacture > option"));
        List<String> result = new ArrayList<String>();
        int j = 0;
        for(int i = 0 ;i< carModels.size();i++){
            if(carModels.get(i).getText().equals("Все марки")){
                j = i;
            }
            else{
                result.add(carModels.get(i).getText().toLowerCase());
            }
        }
        carModels.remove(j);
        logger.info("result before sort:");
        for(String carModel :result){
            logger.info(carModel);
        }

        Collections.sort(result);

        logger.info("result after sort:");
        for(String carModel :result){
            logger.info(carModel);
        }

        sorted = compareLists(carModels,result);

        Assert.assertTrue(sorted);
    }

    private Boolean compareLists(List<WebElement> listOfWebElements,List<String> secondList){
        for(int i= 0 ; i < listOfWebElements.size(); i++){
            String first =  secondList.get(i);
            String second = listOfWebElements.get(i).getText().toLowerCase();
                if (!first.equals(second)){
                    return false;
                }
        }
        return true;
    }

    private List<String> transformToListOfString(List<WebElement> webElementList){

        List<String> webElementsText = new ArrayList<String>();
        for (WebElement webElement : webElementList){
            webElementsText.add(webElement.getText());
        }
        return webElementsText;
    }

    private int returnIndexOfWebElementByText(List<WebElement> webElements, String webElementText){

        int result = -1;

        for(int i =0;i < webElements.size(); i++){

            if(webElements.get(i).getText().contains(webElementText)){

                return i;

            }
        }

        return result;
    }

    @AfterClass
    public static void tearDown() {
        driver.quit();
    }
}
