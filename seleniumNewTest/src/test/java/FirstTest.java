import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;
import java.util.concurrent.TimeUnit;


public class FirstTest {

    private static WebDriver driver;

    @BeforeClass
    public static void setUp() {
        System.setProperty("webdriver.chrome.driver", "/home/azabavskaya/Work/projects/automation/Selenium_2014_November/seleniumNewTest/chromedriver");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @BeforeMethod
    public void getPage() {
        driver.get("http://www.exadel.com/");
    }

    @Test
    public void firstTestImSoExcited() {
        driver.findElement(By.cssSelector(".logo.left")).click();
        Assert.assertEquals("http://www.exadel.com/", driver.getCurrentUrl());
    }

    @Test
    public void testServicesLink() {
        List<WebElement> servicesLinks = driver.findElements(By.xpath("//a[text()='Services']"));
        servicesLinks.get(2).click();
        Assert.assertEquals("http://www.exadel.com/software-application-development/", driver.getCurrentUrl());
    }

    @AfterClass
    public static void tearDown() {
        driver.quit();
    }
}
