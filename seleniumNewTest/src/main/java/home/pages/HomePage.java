package home.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import utils.Helper;

import java.util.List;

/**
 * Created by azabavskaya on 2/1/17.
 */
public class HomePage {

    private WebDriver driver;
    private Logger logger = LoggerFactory.getLogger(HomePage.class);

    private String carMark;
    private String carModel;
    private int firstYear;
    private int secondYear;
    private String motorType;
    private String transmissionText;
    private String color;



    private List<WebElement> searchCarMarks() {

        WebElement webElementManufacture = driver.findElement(By.className("manufacture"));
        webElementManufacture.click();
        logger.info("clicked manufacture");

        return driver.findElements(By.cssSelector(".manufacture > option"));

    }

    private List<WebElement> searchCarModel() {

        WebElement webElementModel = driver.findElement(By.className("model"));
        webElementModel.click();
        logger.info("clicked model");
        return driver.findElements(By.cssSelector(".model > option"));
    }

    private WebElement searchFirstYear() {

        logger.info("select first year");
        return driver.findElement(By.cssSelector("select[name='min-year'] > option[value = '2007']"));
    }

    private WebElement searchSecondYear() {

        logger.info("select second year");
        return driver.findElement(By.cssSelector("select[name='max-year'] > option[value = '2012']"));
    }

    private List<WebElement> searchCarColor() {

        WebElement webElementColor = driver.findElement(By.className("ofm-forms-ips color"));
        webElementColor.click();
        logger.info("clicked color");
        return driver.findElements(By.cssSelector(".ofm-forms-ips color > option"));
    }

    private WebElement searchMotorType() {
        return driver.findElement(By.cssSelector("f-cb"));
    }

    private WebElement getCarMarkSelector(){
        return driver.findElement(By.xpath(".//select[@class = 'manufacture']//option[contains(text(), 'Mitsubishi')]"));
    }

    private WebElement searchTransmission() {

        return driver.findElement(By.cssSelector("input[name = 'transmission[]'][value = '1']"));
    }

    public void selectCarMark(String carMark){
        logger.info(getCarMarkSelector().getText());
        getCarMarkSelector().click();
        this.setCarMark(carMark);

    }

    public void selectCarModel(String carModel) {

        WebElement webElement = driver.findElement(By.xpath(".//select[@class = 'model']//option[contains(text(), 'Lancer')][not(contains(text(),'Evolution'))]"));
        logger.info(webElement.getText());
        webElement.click();
        this.setCarModel(carModel);
    }

    public void selectYearRange(int necessaryFirstYear, int necessarySecondYear) {

        WebElement firstYear = searchFirstYear();
        firstYear.click();
        logger.info("click first year");
        WebElement secondYear = searchSecondYear();
        secondYear.click();
        logger.info("click second year");
        logger.info("select year range");
        this.setYearRange(necessaryFirstYear, necessarySecondYear);
    }

    public void selectCarColor(String carColor) {

        WebElement webElement = driver.findElement(By.xpath(".//div[@class = 'ofm-forms-ips color']//option[contains(text(), 'Красный')]"));
        logger.info(webElement.getText());
        webElement.click();
        Helper helper = new Helper();
        helper.waitForJSandJQueryToLoad(driver);

        this.setColor(carColor);

    }

    public void selectCarMotorType(String motorType) {

        WebElement webElement = driver.findElement(By.name("fuel[]"));
        logger.info(webElement.getText());
        webElement.click();
        this.setMotorType(motorType);
    }



    public HomePage(WebDriver driver) {
        this.driver = driver;
    }

    public void selectTransmission(String transmission){

        WebElement webElement = searchTransmission();
        logger.info(webElement.getText());
        webElement.click();
        this.setTransmissionText(transmission);
    }

    public WebDriver getDriver() {
        return driver;
    }

    public void setDriver(WebDriver driver) {
        this.driver = driver;
    }

    public String getCarMark() {
        return carMark;
    }

    public String getCarModel() {
        return carModel;
    }

    public String getMotorType() {
        return motorType;
    }

    public String getTransmissionText() {
        return transmissionText;
    }

    public String getColor() {
        return color;
    }

    public void setCarMark(String carMark) {
        this.carMark = carMark;
    }

    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    public void setYearRange(int firstYear, int secondYear) {
        this.setFirstYear(firstYear);
        this.setSecondYear(secondYear);

    }

    public void setMotorType(String motorType) {
        this.motorType = motorType;
    }

    public void setTransmissionText(String transmissionText) {
        this.transmissionText = transmissionText;
    }

    public void setColor(String color) {
        this.color = color;

    }

    public int getFirstYear() {
        return firstYear;
    }

    public void setFirstYear(int firstYear) {
        this.firstYear = firstYear;
    }

    public int getSecondYear() {
        return secondYear;
    }

    public void setSecondYear(int secondYear) {
        this.secondYear = secondYear;
    }

    private int returnIndexOfWebElementByText(List<WebElement> webElements, String webElementText){

        int result = -1;
        for(int i =0; i < webElements.size(); i++){

            if(webElements.get(i).getText().contains(webElementText)){

                return i;

            }
        }

        return result;
    }

    private void doClickElementByIndex(List<WebElement> webElementList, int indexOfElement){

        if(indexOfElement >= 0 && indexOfElement < webElementList.size()){

            webElementList.get(indexOfElement).click();
            logger.info(webElementList.get(indexOfElement).getText());

        }
    }
}
