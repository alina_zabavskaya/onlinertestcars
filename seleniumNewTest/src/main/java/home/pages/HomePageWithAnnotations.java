package home.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by azabavskaya on 2/8/17.
 */
public class HomePageWithAnnotations {

    private WebDriver driver;

    private Logger logger = LoggerFactory.getLogger(HomePageWithAnnotations.class);
    private String carMark;
    private String carModel;
    private int firstYearText;
    private int secondYearText;
    private String motorType;
    private String transmissionText;
    private String color;


    public HomePageWithAnnotations(WebDriver driver) {
        this.driver = driver;
    }
    @FindBy(xpath = ".//select[@class = 'manufacture']//option[contains(text(), 'Mitsubishi')]")
    private WebElement mitsubishiWebElement;

    @FindBy(xpath = ".//select[@class = 'model']//option[contains(text(), 'Lancer')][not(contains(text(),'Evolution'))]")
    private WebElement lancerWebElement;

    @FindBy(css = "select[name='min-year'] > option[value = '2007']")
    private WebElement firstYear;

    @FindBy(css = "select[name='max-year'] > option[value = '2012']")
    private WebElement secondYear;

    @FindBy(how = How.CSS, using = ".ofm-forms-checkers.fuels li")
    private List<WebElement> motorTypeList;

    @FindBy(how = How.CSS, using = ".ofm-forms-checkers.transmissions li")
    private List<WebElement> transmissionsList;

    @FindBy(xpath = ".//div[@class = 'ofm-forms-ips color']//option[contains(text(), 'Красный')]")
    private WebElement redWebElement;

    public WebElement getCarMarkSelector(){
        logger.info("get Mitsubishi selector");
        return mitsubishiWebElement;
    }

    public WebElement getCarModelSelector(){
        logger.info("get Lancer selector");
        return lancerWebElement;
    }

    public WebElement getFirstYearSelector() {
        logger.info("get 2007 year selector");
        return firstYear;
    }

    public WebElement getSecondYearSelector() {
        logger.info("get 2012 year selector");
        return secondYear;
    }

    public WebElement getColorSelector(){
        logger.info("get Red selector");
        return redWebElement;
    }

    public void doSelectMitsubishi(String carMark){
        logger.info(getCarMarkSelector().getText() + "click");
        getCarMarkSelector().click();
        this.setCarMark(carMark);

    }

    public void doSelectYearRange(int firstYear, int secondYear){
        logger.info(getFirstYearSelector().getText() + "click");
        getFirstYearSelector().click();
        logger.info(getSecondYearSelector().getText() + "click");
        getSecondYearSelector().click();
        this.setYearRange(firstYear,secondYear);

    }

    private void setYearRange(int firstYear, int secondYear) {

        this.setFirstYearText(firstYear);
        this.setSecondYearText(secondYear);

    }

    public void doSelectLancer(String carMark){
        logger.info(getCarModelSelector().getText() + "click");
        getCarModelSelector().click();
        this.setCarModel(carMark);

    }

    public void doSelectRed(String carColor) {

        logger.info(getColorSelector().getText() + "click");
        getColorSelector().click();
        this.setColor(carColor);

    }

    public void doClickCarMotorTypeByText(String motorType) {

        int index = returnIndexOfWebElementByText(motorTypeList,motorType);
        motorTypeList.get(index).click();
        this.setMotorType(motorType);
    }

    public void setFirstYearText(int firstYearText) {
        this.firstYearText = firstYearText;
    }

    public void setSecondYearText(int secondYearText) {
        this.secondYearText = secondYearText;
    }


    public void doClickTransmissionByText(String motorType) {

        int index = returnIndexOfWebElementByText(transmissionsList,motorType);
        transmissionsList.get(index).click();
        this.setTransmissionText(motorType);

    }

    public void setDriver(WebDriver driver) {
        this.driver = driver;
    }

    public String getCarMark() {
        return carMark;
    }

    public String getCarModel() {
        return carModel;
    }

    public String getMotorType() {
        return motorType;
    }


    public String getTransmissionText() {
        return transmissionText;
    }

    public String getColor() {
        return color;
    }

    public void setCarMark(String carMark) {
        this.carMark = carMark;
    }

    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    public void setMotorType(String motorType) {
        this.motorType = motorType;
    }

    public void setTransmissionText(String transmissionText) {
        this.transmissionText = transmissionText;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getFirstYearText() {
        return firstYearText;
    }

    public int getSecondYearText() {
        return secondYearText;
    }

    public List<WebElement> getMotorTypeList() {
        return motorTypeList;
    }


    public void setMotorTypeList(List<WebElement> motorTypeList) {
        this.motorTypeList = motorTypeList;
    }

    private int returnIndexOfWebElementByText(List<WebElement> webElements, String webElementText){

        int result = -1;
        for(int i =0; i < webElements.size(); i++){

            if(webElements.get(i).getText().contains(webElementText)){

                return i;

            }
        }

        return result;
    }


    private void doClickElementByIndex(List<WebElement> webElementList, int indexOfElement){

        if(indexOfElement >= 0 && indexOfElement < webElementList.size()){

            webElementList.get(indexOfElement).click();
            logger.info(webElementList.get(indexOfElement).getText());

        }
    }
}
