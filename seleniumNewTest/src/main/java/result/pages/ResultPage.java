package result.pages;

import home.pages.HomePage;
import home.pages.HomePageWithAnnotations;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;


/**
 * Created by azabavskaya on 2/1/17.
 */
public class ResultPage {

    private WebDriver driver;
    private String carMark;
    private String carModel;
    private String motorType;
    private String transmissionText;
    private String color;
    private int firstYear;
    private int secondYear;

    private Logger logger = LoggerFactory.getLogger(ResultPage.class);


    public ResultPage(WebDriver driver) {
        this.driver = driver;
    }

    public ResultPage(HomePage homePage) {

        this.driver = homePage.getDriver();
        this.carMark = homePage.getCarMark();
        this.carModel = homePage.getCarModel();
        this.motorType = homePage.getMotorType();
        this.transmissionText = homePage.getTransmissionText();
        this.color = homePage.getColor();
        this.firstYear = homePage.getFirstYear();
        this.secondYear = homePage.getSecondYear();
    }

    public ResultPage(HomePageWithAnnotations homePageWithAnnotations) {

        this.carMark = homePageWithAnnotations.getCarMark();
        this.carModel = homePageWithAnnotations.getCarModel();
        this.motorType = homePageWithAnnotations.getMotorType();
        this.transmissionText = homePageWithAnnotations.getTransmissionText();
        this.color = homePageWithAnnotations.getColor();
        this.firstYear = homePageWithAnnotations.getFirstYearText();
        this.secondYear = homePageWithAnnotations.getSecondYearText();
    }

    private int findCarYear(WebElement webElement){

        WebElement webElementYear = webElement.findElement(By.className("year"));
        String textFromWebElementYear = webElementYear.getText();
        logger.info(webElementYear.getText());

        logger.info("check year");

        return Integer.parseInt(textFromWebElementYear);
    }

    private String findCarMarkAndModel(WebElement webElement){

        WebElement webElementYear = webElement.findElement(By.cssSelector("a > strong"));
        String textFromWebElementYear = webElementYear.getText();
        logger.info("check mark and model");

        return textFromWebElementYear;
    }

    private String otherParameters(WebElement webElement){
        WebElement webElementYear = webElement.findElement(By.tagName("p"));
        String textFromWebElementYear = webElementYear.getText();
        logger.info("check other parameters");

        return textFromWebElementYear;
    }

    public boolean doCheckCarParameters(int firstYear, int secondYear){

        List<WebElement> webElement = driver.findElements(By.className("carRow"));

        if(webElement.isEmpty()){
            return false;
        }

        for(int i = 1; i < webElement.size(); i++){

            WebElement currentWebElement =  webElement.get(i);
            int year = findCarYear(currentWebElement);
            String markAndModel = findCarMarkAndModel(currentWebElement);
            String otherParameters = otherParameters(currentWebElement);

            if(firstYear <= year && year <= secondYear){
                logger.info("result year is in range");
            }else{
                logger.info(Integer.toString(year));
                logger.info("year is not in range");
                return false;
            }

            if(markAndModel.contains("Mitsubishi Lancer")){
                logger.info("result contains correct mark and model");
            }else{
                logger.info("mark or model is incorrect");
                return false;
            }

            if(otherParameters.contains("бензин")){
                logger.info("result contains correct transmission type");
            }else{
                logger.info("other parameters is incorrect");
                return false;
            }

            if(otherParameters.contains("автомат")){
                logger.info("result contains correct transmission");
            }else{
                logger.info("other parameters is incorrect");
                return false;
            }
        }

        logger.info("all cars are checked");
        return true;
    }

    public String getCarMark() {
        return carMark;
    }

    public void setCarMark(String carMark) {
        this.carMark = carMark;
    }

    public String getCarModel() {
        return carModel;
    }

    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    public String getMotorType() {
        return motorType;
    }

    public void setMotorType(String motorType) {
        this.motorType = motorType;
    }

    public String getTransmissionText() {
        return transmissionText;
    }

    public void setTransmissionText(String transmission) {
        this.transmissionText = transmission;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getFirstYear() {
        return firstYear;
    }

    public void setFirstYear(int firstYear) {
        this.firstYear = firstYear;
    }

    public int getSecondYear() {
        return secondYear;
    }

    public void setSecondYear(int secondYear) {
        this.secondYear = secondYear;
    }
}
